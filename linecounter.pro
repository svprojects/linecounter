CONFIG += console
TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

linux {
    #not sure about maintained
    CONFIG += c++17
    LIBS += -lstdc++fs
}
macos {
    #dont know about clion but trust it
    CONFIG += c++17
}
win32 {
    #msvc2017 only
    CONFIG += c++17
}

SOURCES += main.cpp

HEADERS += \
    clara.hpp \
    lcounter.hpp



