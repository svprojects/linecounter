Счетчик строк в файлах проектов.

Пример использования:

 linecounter.exe -p ./ -e ".*(.h|.cpp|.pro|.pri)" -X ".*(.git|build)" > /c/tmp/statistic.txt
