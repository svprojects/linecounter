#ifndef LCOUNTER_HPP
#define LCOUNTER_HPP

#include <stdlib.h>
#include <iostream>
#include <experimental/filesystem>
#include <fstream>
#include <iterator>
#include <string>
#include <regex>

namespace lcounter {

using namespace std;
namespace fs = std::experimental::filesystem;

const string DEFAULT_EXTENSION = ".*";

class Counter {
    auto regex_vailidate(string reg) -> bool {
        try { regex tmp(reg); }
        catch (regex_error&) { return false; }
        return true;
    }

    auto vaildate() -> bool {
        if(!regex_vailidate(extension)) {
            cerr << "Error in extension. Cant parse reg value." << endl;
            return false;
        }
        if(!regex_vailidate(extension_exclude)) {
            cerr << "Error in exclued extension. Cant parse reg value." << endl;
            return false;
        }
        if(!regex_vailidate(extension_exclude_path)) {
            cerr << "Error in path extension. Cant parse reg value." << endl;
            return false;
        }
        return true;
    }

public:
    string root_path;
    string extension;
    string extension_exclude;
    string extension_exclude_path;

    explicit Counter() : Counter("", DEFAULT_EXTENSION) {}
    explicit Counter(const string& path, const string ext) : root_path(path), extension(ext) {}

    auto count_path() -> int {
        if(!vaildate()) return -1;

        fs::path path = root_path;
        cout << path << endl;
        if(fs::is_directory(path)) return count_dir(path);
        else if(fs::is_regular_file(path)) return count_file(path);
        else {
            cout << "Cant parse current path.";
            return -1;
        }
    }

    auto count_file(const fs::path p) -> int {
        ifstream in(p);
        return count(istreambuf_iterator<char>(in), istreambuf_iterator<char>(), '\n');
    }

    auto count_dir(const fs::path path,int deep=0) -> int {
        auto tab = string(2*deep, ' ');
        int sum = 0;
        for(auto& p : fs::directory_iterator(path)) {
            if(fs::is_regular_file(p)) {
                string ext = p.path().extension().string();
                if(regex_match(ext, regex(extension)) &&
                        !regex_match(ext, regex(extension_exclude)) &&
                        !regex_match(p.path().string(), regex(extension_exclude_path))) {
                    int c = count_file(p);
                    sum+=c;
                    cout << tab << "  |- "<< p.path().filename() << " (" << c << ") " << endl;
                }
            }
            else if (fs::is_directory(p)) {
                if(regex_match(p.path().string(), regex(extension_exclude_path))) continue;
                cout << tab << "  |- " << p.path().filename() << endl;
                sum+=count_dir(p, deep+1);
            }
        }
        cout << tab << "  SUM: " << sum << endl;
        return sum;
    }

};


}
#endif // LCOUNTER_HPP
