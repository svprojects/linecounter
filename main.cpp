#include <stdlib.h>
#include <iostream>
#include <string>

#include "lcounter.hpp"
#include "clara.hpp"

using namespace std;

void print_help() {
    cout << "linesounter use: linecounter [flags]" << endl;
    cout << "    -p (--path) - path for count lines." << endl;
    cout << "    -e (--extension) - right extension for count." << endl;
    cout << "    -x (--extension-exclude) - wrong extension for count." << endl;
    cout << "    -X (--extension-exclude-path) - wrong pathes for count." << endl;
    cout << endl << "For example: " << endl;
    cout << " linecounter "
            "-p ./ "
            "-e \".*(.h|.cpp|.pro|.pri)\" "
            "-x \".pb.cpp\""
            "-X \".*(.git|build)\"" << endl;
}

int main(int argc, char *argv[])
{
    auto lc = lcounter::Counter();

    bool is_help = false;
    auto cli = clara::Opt(lc.root_path, "path")
            ["-p"]["--path"]
            ("Choose fath of file or directory") |
            clara::Opt(lc.extension, "extension")
            ["-e"]["--extension"]
            ("Choose extension of count file") |
            clara::Opt(lc.extension_exclude, "extension_exclude")
            ["-x"]["--extension-exclude"]
            ("Choose extension of count file is exclude.") |
            clara::Opt(lc.extension_exclude_path, "extension_exclude_path")
            ["-X"]["--extension-exclude-path"]
            ("Choose dirs of count file is exclude.") |
            clara::Help(is_help);
    auto result_parse = cli.parse(clara::Args(argc, argv));
    if(!result_parse) {
        cerr << "Error in command line: " << result_parse.errorMessage() << endl;
        exit(1);
    }
    if(is_help) {
        print_help();
        exit(0);
    }

    lc.count_path();

    return 0;
}
